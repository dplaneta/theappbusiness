//
//  ABEmployeeListViewController.h
//  Who's Who
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABEmployeeListModelController;
@interface ABEmployeeListViewController : UIViewController
- (instancetype)initWithModelController:(id<ABEmployeeListModelController>)modelController NS_DESIGNATED_INITIALIZER;
@end
