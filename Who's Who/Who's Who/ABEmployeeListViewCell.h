//
//  ABEmployeeListViewCell.h
//  Who's Who
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABEmployee;
@interface ABEmployeeListViewCell : UITableViewCell
@property (nonatomic, strong) ABEmployee *employee;
- (void)setImage:(UIImage*)image;
+ (CGSize)preferableImageSize;
@end
