//
//  ABEmployeeListViewCell.m
//  Who's Who
//
//  Warning:
//  Performance of rendering this cell is extremaly important for the application thus don't use constraints.
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "ABEmployeeListViewCell.h"
#import "ABEmployeeListViewCell_Protected.h"
#import "ABEmployee.h"

@interface ABEmployeeListViewCell ()
@property (nonatomic, strong) CALayer *imageLayer;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
@property (nonatomic, strong) UILabel *name;
@property (nonatomic, strong) UILabel *jobTitle;
@property (nonatomic, strong) UITextView *biography;
@end

@implementation ABEmployeeListViewCell{
    UIEdgeInsets insets;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        insets = UIEdgeInsetsMake(10, 10, 10, 10);
        
        _imageLayer = [CALayer layer];
        [_imageLayer setDrawsAsynchronously:YES];
        [_imageLayer setContentsScale:[[UIScreen mainScreen] scale]];
        [self.contentView.layer addSublayer:_imageLayer];
        
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [_spinner startAnimating];
        [self.contentView addSubview:_spinner];
        
        _name = [UILabel new];
        [_name setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0f]];
        [self.contentView addSubview:_name];
        
        _jobTitle = [UILabel new];
        [_jobTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:13.0f]];
        [self.contentView addSubview:_jobTitle];
        
        _biography = [UITextView new];
        [_biography setUserInteractionEnabled:NO];
        [_jobTitle setFont:_jobTitle.font];
        [self.contentView addSubview:_biography];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self updateContentSizeForWidth:self.bounds.size.width];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    [_imageLayer setContents:nil];
    [_spinner startAnimating];
}


#pragma mark - Setter

- (void)setEmployee:(ABEmployee *)employee
{
    _employee = employee;
    [_name setText:employee.name];
    [_jobTitle setText:employee.jobTitle];
    [_biography setText:employee.biography];
}


#pragma mark - Public API

- (void)setImage:(UIImage*)image
{
    [_imageLayer setContentsGravity:kCAGravityCenter];
    [_imageLayer setContents:(__bridge id)image.CGImage];
    [_spinner stopAnimating];
}

+ (CGSize)preferableImageSize
{
    return CGSizeMake(100.0f, 100.0f);
}


#pragma mark - Protected API

// I'm not using [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
// Because is too slow for me.
- (CGFloat)fitHeightForWidth:(CGFloat)width
{
    [self updateContentSizeForWidth:width];
    return _biography.frame.origin.y + _biography.frame.size.height + insets.bottom;
}


#pragma mark - Private API

- (void)updateContentSizeForWidth:(CGFloat)width
{
    // Warning.
    // For performance issues (to support perfect smooth of scroll view)
    // I decided to set couple of views manually instant  of use constraints.
    CGSize imageSize = [[self class] preferableImageSize];
    [_imageLayer setFrame:(CGRect){CGPointMake(insets.left, insets.top), imageSize}];
    
    [_spinner setCenter:CGPointMake(insets.left+0.5f*imageSize.width, insets.top+0.5f*imageSize.height)];
    
    float x = _imageLayer.frame.origin.x+_imageLayer.frame.size.width+insets.left;
    [_name setFrame:CGRectMake(x, _imageLayer.frame.origin.y, width-(x+insets.right), 20)];
    [_jobTitle setFrame:(CGRect){CGPointMake(_name.frame.origin.x, _name.frame.origin.y+_name.frame.size.height+insets.top), _name.frame.size}];
    
    [_biography setFrame:CGRectMake(insets.left, _imageLayer.frame.origin.y+_imageLayer.frame.size.height+insets.top, width-(insets.left+insets.right), 0)];
    [_biography sizeToFit];
}

@end
