//
//  ABEmployeeListViewCell_Protected.h
//  Who's Who
//
//  Created by Dawid on 04/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "ABEmployeeListViewCell.h"

@interface ABEmployeeListViewCell ()

// I'm not using [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
// Because is too slow for me.
- (CGFloat)fitHeightForWidth:(CGFloat)width;

@end
