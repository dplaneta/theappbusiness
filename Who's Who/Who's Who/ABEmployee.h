//
//  ABEmployee.h
//  Who's Who
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABEmployee : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *jobTitle;
@property (nonatomic, strong) NSString *biography;
@property (nonatomic, strong) NSURL *photoURL;
@end
