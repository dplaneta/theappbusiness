//
//  ABEmployeeListModelController.m
//  Who's Who
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "DTHTMLParser.h"
#import "ABEmployeeListModelController.h"
#import "ABEmployeeListViewCell_Protected.h"
#import "ABEmployee.h"

@interface ABEmployeeListModelController () <DTHTMLParserDelegate>
@property (nonatomic, strong) NSArray* (^bridge)(NSArray*);
@property (nonatomic, strong) NSOperationQueue *queue;
@property (nonatomic, strong) NSMutableArray *employees;
@property (nonatomic, strong) NSURL *url;
@end


@implementation ABEmployeeListModelController{
    ABEmployee *pCurrentElement;
    NSMutableString *pCurrentElementData;
    SEL selectorName;
}
@synthesize delegate = _delegate;

- (instancetype)initWithUrl:(NSURL*)url
{
    self = [super init];
    if(self){
        // The queue is used only to fetch html page.
        _queue     = [NSOperationQueue new];
        _url       = url;
        _employees = [NSMutableArray new];
    }
    return self;
}


#pragma mark - Public API

- (ABEmployee*)employeeForIndex:(NSUInteger)index
{
    return _employees[index];
}

- (NSUInteger)numberOfEmployees
{
    return [_employees count];
}

- (void)fetchDataWithBridge:(NSArray *(^)(NSArray *employees))bridge
{
    self.bridge = [bridge copy];
    [self.queue cancelAllOperations];
    if(nil==_url){
        if([self.delegate respondsToSelector:@selector(modelController:didFailWithError:)]){
            NSError *error = [NSError errorWithDomain:NSLocalizedString(@"Incorrect URL has been used!", nil) code:0 userInfo:nil];
            [self.delegate modelController:self didFailWithError:error];
        }
    }
    else if(self.delegate){
        __weak typeof (self) weakSelf = self;
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:_url cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:20.0];
        [request setValue:@"no-cache" forHTTPHeaderField:@"cache-control"];
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:self.queue
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
                                   if(error){
                                       __strong typeof (self) strongSelf = weakSelf;
                                       if(strongSelf && [strongSelf.delegate respondsToSelector:@selector(modelController:didFailWithError:)]){
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               [strongSelf.delegate modelController:strongSelf didFailWithError:error];
                                           });
                                       }
                                   }else{
                                       [weakSelf didReceiveData:data];
                                   }
                               }];
    }
}

- (void)cancel
{
    [_queue cancelAllOperations];
}


#pragma mark - Protected API

- (void)didReceiveData:(NSData *)data
{
    DTHTMLParser *parser = [[DTHTMLParser alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [parser setDelegate:self];
    [parser parse];
    pCurrentElement = nil;
	pCurrentElementData = nil;
}


#pragma mark - DTHTMLParserDelegate

- (void)parser:(DTHTMLParser *)parser didStartElement:(NSString *)elementName attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:@"div"]){
        NSString *sClass = attributeDict[@"class"];
        if(sClass && [[sClass componentsSeparatedByString:@" "] containsObject:@"col2"]){
            pCurrentElement = [ABEmployee new];
            [_employees addObject:pCurrentElement];
        }
    }
    if(pCurrentElement){
        if([elementName isEqualToString:@"img"]){
            [pCurrentElement setPhotoURL:[NSURL URLWithString:attributeDict[@"src"]]];
        }
        else if([elementName isEqualToString:@"h3"]){
            selectorName = NSSelectorFromString(@"name");
        }
        else if(pCurrentElement.name && [elementName isEqualToString:@"p"]){
            if([attributeDict[@"class"] isEqualToString:@"user-description"]){
                selectorName = NSSelectorFromString(@"biography");
            }
            else if(!pCurrentElement.jobTitle){
                selectorName = NSSelectorFromString(@"jobTitle");
            }
            else{
                selectorName = nil;
            }
        }
    }
}

- (void)parser:(DTHTMLParser *)parser foundCharacters:(NSString *)string
{
	if(!pCurrentElementData) pCurrentElementData = [NSMutableString new];
	[pCurrentElementData appendString:string];
}

- (void)parser:(DTHTMLParser *)parser didEndElement:(NSString *)elementName
{
    if(selectorName){
        if ([pCurrentElement respondsToSelector:selectorName]) {
            NSString *value = [pCurrentElementData stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [pCurrentElement setValue:value forKey:NSStringFromSelector(selectorName)];
        }
    }
    selectorName = nil;
    pCurrentElementData = nil;
}

- (void)parserDidEndDocument:(DTHTMLParser *)parser
{
    NSAssert([self.delegate respondsToSelector:@selector(modelController:didReceiveData:)], @"Method is required by protocol");
    if(self.bridge){
        self.employees = [[NSMutableArray alloc] initWithArray:self.bridge(self.employees)];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.delegate modelController:self didReceiveData:self.employees];
    });
}

- (void)parser:(DTHTMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
     NSString *info = [NSString stringWithFormat:@"Error %li, Description: %@, Line: %li, Column: %li", (long)[parseError code], [[parser parserError] localizedDescription], (long)[parser lineNumber], (long)[parser columnNumber]];
    NSLog(@"parseErrorOccurred: %@", info);
}

@end
