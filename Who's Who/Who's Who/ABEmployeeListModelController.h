//
//  ABEmployeeListModelController.h
//  Who's Who
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ABEmployeeListModelController;
@protocol ABEmployeeListModelControllerFetchDataDelegate <NSObject>
@required
- (void)modelController:(id<ABEmployeeListModelController>)modelController didReceiveData:(NSArray*)employees;
@optional
- (void)modelController:(id<ABEmployeeListModelController>)modelController didFailWithError:(NSError *)error;
@end


@class ABEmployee;
@protocol ABEmployeeListModelController <NSObject>
@property id<ABEmployeeListModelControllerFetchDataDelegate> delegate;
- (ABEmployee*)employeeForIndex:(NSUInteger)index;
- (NSUInteger)numberOfEmployees;
- (void)fetchDataWithBridge:(NSArray *(^)(NSArray *employees))bridge;
- (void)cancel;
@end

@interface ABEmployeeListModelController : NSObject <ABEmployeeListModelController>
- (instancetype)initWithUrl:(NSURL*)url NS_DESIGNATED_INITIALIZER;
@end
