//
//  ABEmployeeListModelController_Protected.h
//  Who's Who
//
//  Created by Dawid on 04/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "ABEmployeeListModelController.h"

@interface ABEmployeeListModelController ()
- (void)didReceiveData:(NSData *)data;
@end
