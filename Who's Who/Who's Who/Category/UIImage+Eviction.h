//
//  UIImage+Eviction.h
//  Who's Who
//
//  Created by Dawid on 04/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Eviction) <NSDiscardableContent>

@end
