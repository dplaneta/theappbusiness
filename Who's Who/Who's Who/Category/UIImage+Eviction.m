//
//  UIImage+Eviction.m
//  Who's Who
//
//  Created by Dawid on 04/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "UIImage+Eviction.h"

@implementation UIImage (Eviction)

- (void)endContentAccess{}
- (void)discardContentIfPossible{}
- (BOOL)beginContentAccess
{
    return YES;
}
- (BOOL)isContentDiscarded
{
    return NO;
}

@end
