//
//  ABEmployeeListViewController.m
//  Who's Who
//
//  Created by Dawid on 03/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import "ABEmployeeListViewController.h"
#import "ABEmployeeListModelController.h"
#import "ABEmployeeListViewCell.h"
#import "ABEmployeeListViewCell_Protected.h"
#import "ABEmployee.h"
#import "UIImage+Crop.h"
#import "UIImage+Eviction.h"

static NSString * const dequeueCellIdentifier = @"ABEmployeeListViewCellIdentifier";

@interface ABEmployeeListViewController () <ABEmployeeListModelControllerFetchDataDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) id<ABEmployeeListModelController> modelController;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSOperationQueue *loadingOperationQueue;
@property (nonatomic, strong) NSMutableDictionary *downloadOperations;
@property (nonatomic, strong) NSCache *cache;
@property (nonatomic, strong) ABEmployeeListViewCell *prototypeCell;

- (void)fetchData;
- (void)fetchImage:(NSURL*)url preferedImageSize:(CGSize)preferedImageSize indexPath:(NSIndexPath *)indexPath;
- (void)didEndDisplayingEmployeeViewForUrl:(NSURL*)url;
- (void)setImage:(UIImage*)image tableView:(UITableView*)tableView cellIndexPath:(NSIndexPath*)indexPath;
@end



@implementation ABEmployeeListViewController

- (instancetype)initWithModelController:(id<ABEmployeeListModelController>)modelController
{
    self = [super init];
    if(self){
        self.cache = [NSCache new];
        [self.cache setCountLimit:200];
        [self.cache setEvictsObjectsWithDiscardedContent:YES];
        
        self.downloadOperations    = [NSMutableDictionary new];
        self.loadingOperationQueue = [NSOperationQueue new];
        [self.loadingOperationQueue setMaxConcurrentOperationCount:2];
        
        self.modelController = modelController;
        [self.modelController setDelegate:self];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    [self.tableView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.tableView setRowHeight:300.0f];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.tableView setHidden:YES];
    [self.tableView setAllowsSelection:NO];
    [self.tableView setContentInset:UIEdgeInsetsMake(21, 0, 0, 0)];
    [self.tableView registerClass:[ABEmployeeListViewCell class] forCellReuseIdentifier:dequeueCellIdentifier];
    [self.view addSubview:self.tableView];
    
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [self.spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.spinner startAnimating];
    [self.view addSubview:self.spinner];
    
    self.prototypeCell = [[ABEmployeeListViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    [self fetchData];
}


#pragma mark - UIConstraintBasedLayoutCoreMethods

- (void)updateViewConstraints
{
    // TableView
    NSLayoutConstraint *width =[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:0 toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0];
    NSLayoutConstraint *height =[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:0 toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0];
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.f];
    NSLayoutConstraint *leading = [NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.f];
    [self.view addConstraints:@[width, height, top, leading]];
    
    // Spinner
    NSLayoutConstraint *spinnerCenterY =[NSLayoutConstraint constraintWithItem:self.spinner attribute:NSLayoutAttributeCenterY relatedBy:0 toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
    NSLayoutConstraint *spinnerCenterX =[NSLayoutConstraint constraintWithItem:self.spinner attribute:NSLayoutAttributeCenterX relatedBy:0 toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
    [self.view addConstraints:@[spinnerCenterX, spinnerCenterY]];
    
    [super updateViewConstraints];
}


#pragma mark - ABEmployeeListModelControllerFetchDataDelegate

- (void)modelController:(id<ABEmployeeListModelController>)modelController didReceiveData:(NSArray*)employees
{
    [self.spinner stopAnimating];
    [self.tableView setHidden:NO];
    [self.tableView reloadData];
}

- (void)modelController:(id<ABEmployeeListModelController>)modelController didFailWithError:(NSError *)error
{
    [self.spinner stopAnimating];
    [[[UIAlertView alloc] initWithTitle:@"Connection failed!"
                                message:[NSString stringWithFormat:@"Error - %@ %@", [error localizedDescription], [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]]
                               delegate:self
                      cancelButtonTitle:@"Try again"
                      otherButtonTitles:nil] show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self fetchData];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.modelController numberOfEmployees];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ABEmployee *employee = [self.modelController employeeForIndex:indexPath.row];
    ABEmployeeListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:dequeueCellIdentifier];
    [cell setEmployee:employee];
    [self fetchImage:employee.photoURL preferedImageSize:[ABEmployeeListViewCell preferableImageSize] indexPath:indexPath];
    return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath
{
    ABEmployee *employee = [self.modelController employeeForIndex:indexPath.row];
    [self didEndDisplayingEmployeeViewForUrl:employee.photoURL];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // I'm not using [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    // Because is too slow for me.
    ABEmployee *employee = [self.modelController employeeForIndex:indexPath.row];
    [self.prototypeCell setEmployee:employee];
    return [self.prototypeCell fitHeightForWidth:self.view.bounds.size.width];
}


#pragma mark - Protected API

- (void)fetchData
{
    [self.spinner startAnimating];
    [self.tableView setHidden:YES];
    [self.modelController fetchDataWithBridge:^NSArray *(NSArray *employees) {
        NSMutableIndexSet *discardedEmployess = [NSMutableIndexSet indexSet];
        NSMutableArray *tmpEmployess = [[NSMutableArray alloc] initWithArray:employees];
        [tmpEmployess enumerateObjectsUsingBlock:^(ABEmployee *employe, NSUInteger idx, BOOL *stop) {
            if(0==[employe.name length]){
                [discardedEmployess addIndex:idx];
            }
        }];
        [tmpEmployess removeObjectsAtIndexes:discardedEmployess];
        return [tmpEmployess copy];
    }];
}

- (void)fetchImage:(NSURL*)url preferedImageSize:(CGSize)preferedImageSize indexPath:(NSIndexPath *)indexPath
{
    if(url){
        if([self.cache objectForKey:url]){
            [self setImage:[self.cache objectForKey:url] tableView:self.tableView cellIndexPath:indexPath];
        }
        else{
            NSBlockOperation *loadImageIntoEmployeeViewCell = [NSBlockOperation new];
            __weak NSBlockOperation *weakOp = loadImageIntoEmployeeViewCell;
            __weak typeof (self) weakSelf = self;
            [loadImageIntoEmployeeViewCell addExecutionBlock:^(void){
                NSError *error = NULL;
                UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:&error]];
                
                __strong typeof (self) strongSelf = weakSelf;
                if(strongSelf){
                    [strongSelf.downloadOperations removeObjectForKey:url];
                    if(image){
                        image = [image imageCroppedAndScaledToSize:preferedImageSize contentMode:UIViewContentModeScaleAspectFit circle:YES];
                        [strongSelf.cache setObject:image forKey:url];
                        [strongSelf setImage:image tableView:strongSelf.tableView cellIndexPath:indexPath];
                    }
                }
            }];
            
            [self.downloadOperations setObject:weakOp forKey:url];
            [self.loadingOperationQueue addOperation:weakOp];
        }
    }
}

- (void)didEndDisplayingEmployeeViewForUrl:(NSURL*)url
{
    //Fetch operation that doesn't need executing anymore
    NSBlockOperation *ongoingDownloadOperation = [self.downloadOperations objectForKey:url];
    if (ongoingDownloadOperation) {
        [ongoingDownloadOperation cancel];
        [self.downloadOperations removeObjectForKey:url];
        NSLog(@"Cancel fetch image execute: %@", [url absoluteString]);
    }
}

- (void)setImage:(UIImage*)image tableView:(UITableView*)tableView cellIndexPath:(NSIndexPath*)indexPath
{
    [[NSOperationQueue mainQueue] addOperationWithBlock:^(void) {
        if([[tableView indexPathsForVisibleRows] containsObject:indexPath]){
            ABEmployeeListViewCell *cell = (ABEmployeeListViewCell*)[tableView cellForRowAtIndexPath:indexPath];
            [cell setImage:image];
        }
    }];
}

@end
