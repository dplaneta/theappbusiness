//
//  ABEmployeeListModelControllerTest.m
//  Who's Who
//
//  Created by Dawid on 04/08/2014.
//  Copyright (c) 2014 Dawid. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ABEmployeeListModelController.h"
#import "ABEmployeeListModelController_Protected.h"
#import "ABEmployee.h"

@interface ABEmployeeListModelControllerTest : XCTestCase <ABEmployeeListModelControllerFetchDataDelegate>
@property (nonatomic, strong) ABEmployeeListModelController *modelController;
@property (nonatomic, strong) NSData *mockupPage;
@end

@implementation ABEmployeeListModelControllerTest{

}

- (void)setUp
{
    [super setUp];
    self.mockupPage = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"aboutus" ofType:@"mockup"]];
    self.modelController = [[ABEmployeeListModelController alloc] initWithUrl:nil];
    [self.modelController setDelegate:self];
    [self.modelController didReceiveData:self.mockupPage];
}

- (void)testExistingMockup
{
    XCTAssertNotNil(self.mockupPage);
}

- (void)testConstructor
{
    XCTAssertNotNil(self.modelController);
}

- (void)testNumberOfEmployess
{
    const NSUInteger mockupNumberOfEmployees = 43;
    XCTAssertTrue(mockupNumberOfEmployees == [self.modelController numberOfEmployees], @"Wrong number of employess!");
}

- (void)testEmployeeData
{
    ABEmployee *employeeTest = [ABEmployee new];
    employeeTest.name = @"Daniel Joseph";
    employeeTest.jobTitle = @"Founder, Strategy Director";
    employeeTest.biography = @"Daniel was previously European Planning Director for Apple at Media Arts Lab, launching the iPhone, App Store and hundreds of apps. Prior to this, Daniel spent his career planning advertising and communications for Sony Computer Entertainment Europe and Mars. He has a BA in Geography from Keble College, Oxford.";
    employeeTest.photoURL = [NSURL URLWithString:@"http://www.theappbusiness.com/wp-content/uploads/2013/06/dan-e1372264031694.jpg"];
    
    ABEmployee *employee = [self.modelController employeeForIndex:0];
    
    XCTAssertTrue([employeeTest.name isEqualToString:employee.name],           @"EmployeeData is not correct because of name");
    XCTAssertTrue([employeeTest.jobTitle isEqualToString:employee.jobTitle],   @"EmployeeData is not correct because of jobTitle");
    XCTAssertTrue([employeeTest.biography isEqualToString:employee.biography], @"EmployeeData is not correct because of photoURL");
    XCTAssertTrue([employeeTest.photoURL isEqual:employee.photoURL],           @"EmployeeData is not correct because of name");
}



#pragma mark - ABEmployeeListModelControllerFetchDataDelegate

- (void)modelController:(id<ABEmployeeListModelController>)modelController didReceiveData:(NSArray*)employees
{
}

@end
